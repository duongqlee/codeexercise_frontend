import React from "react";
import { useLoaderData } from "react-router-dom";

export async function loadRide({ params }) {
  const ride = await fetch(`http://localhost:8010/rides/${params.id}`);
  return ride;
}

export default function Ride() {
  const ride = useLoaderData();

  return (
    <>
      <h2>Ride details</h2>
      <ul>
        <li>ID: {ride.rideID}</li>
        <li>Start latitude: {ride.startLat}</li>
        <li>Start longtitude: {ride.startLong}</li>
        <li>End latitude: {ride.endLat}</li>
        <li>End longtitude: {ride.endLong}</li>
        <li>Rider name: {ride.riderName}</li>
        <li>Driver name: {ride.driverName}</li>
        <li>Vehicle: {ride.driverVehicle}</li>
      </ul>
    </>
  );
}
