import React from "react";
import Header from "./Header";

export default function Home() {
  return (
    <>
      <Header />
      <h1>Rides system</h1>
    </>
  );
}
