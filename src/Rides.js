import React from "react";
import Header from "./Header";
import { Link, Outlet, useLoaderData } from "react-router-dom";

export async function loadRides() {
  const rides = await fetch("http://localhost:8010/rides");
  return rides;
}

export default function Rides() {
  const rides = useLoaderData();
  const rideList = rides.results.map((ride) => (
    <li key={ride.rideID}>
      <Link to={`${ride.rideID}`}>ID: {ride.rideID}</Link>
    </li>
  ));

  return (
    <>
      <Header />
      <h2>Rides</h2>
      <ul>{rideList}</ul>
      <div>
        <Outlet />
      </div>
    </>
  );
}
