import React from "react";
import { NavLink } from "react-router-dom";

export default function Header() {
  return (
    <nav>
      <ul>
        <li>
          <NavLink
            to="/"
            className={({ isActive }) => (isActive ? "active" : "")}
          >
            Home
          </NavLink>
        </li>
        <li>
          <NavLink
            to="/rides"
            className={({ isActive }) => (isActive ? "active" : "")}
          >
            Ride list
          </NavLink>
        </li>
        <li>
          <NavLink
            to="/rides/create"
            className={({ isActive }) => (isActive ? "active" : "")}
          >
            Create a new ride
          </NavLink>
        </li>
        <li>
          <NavLink
            to="/rides"
            className={({ isActive }) => (isActive ? "active" : "")}
          >
            Update ride
          </NavLink>
        </li>
        <li>
          <NavLink
            to="/rides"
            className={({ isActive }) => (isActive ? "active" : "")}
          >
            Map
          </NavLink>
        </li>
      </ul>
    </nav>
  );
}
