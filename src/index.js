import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import Home from "./Home";
import Rides, { loadRides } from "./Rides";
import Ride, { loadRide } from "./Ride";
import RideIndex from "./RideIndex";

import { createBrowserRouter, RouterProvider } from "react-router-dom";

const router = createBrowserRouter([
  {
    path: "/",
    element: <Home />,
  },
  {
    path: "/rides",
    element: <Rides />,
    loader: loadRides,
    children: [
      {
        index: true,
        element: <RideIndex />,
      },
      {
        path: ":id",
        element: <Ride />,
        loader: loadRide,
      },
    ],
  },
  {
    path: "/rides/create",
    element: <Rides />,
    loader: loadRides,
    children: [
      {
        index: true,
        element: <RideIndex />,
      },
      {
        path: ":id",
        element: <Ride />,
        loader: loadRide,
      },
    ],
  },
  {
    path: "/rides/update",
    element: <Rides />,
    loader: loadRides,
    children: [
      {
        index: true,
        element: <RideIndex />,
      },
      {
        path: ":id",
        element: <Ride />,
        loader: loadRide,
      },
    ],
  },
]);

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>
);
